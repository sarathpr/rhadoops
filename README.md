RHadoops
==========

General Notes
---------------

The name RHadoops is kept as a short for RHadoop Experiments / RHadoop programs

RHadoops are examples for Bigdata analysis using R.

These examples are using Revolution Analytics's rhdfs, rmr and rhbase packages. 

The programs are tested on following versions of R and Hadoop

Tested on both CDH 4.2.1 and CDH 4.3.0 with R 2.15.1 and R 3.0.1
 
More information on RHadoop packages can be found [here](https://github.com/RevolutionAnalytics/RHadoop/wiki)

[Blog Post](http://sprism.blogspot.in/2013/08/r-and-hadoop.html)
